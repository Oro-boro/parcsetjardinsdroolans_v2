/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)

import './styles/normalize.css'
import './styles/style.css'
import './js/navBarScroll'
// start the Stimulus application
import './bootstrap'


// Modal Image Gallery
global.onClick = function onClick(element) {
  document.getElementById('img01').src = element.src
  document.getElementById('modal01').style.display = 'block'
  var captionText = document.getElementById('caption')
  captionText.innerHTML = element.alt
}

// Used to toggle the menu on small screens when clicking on the menu button
global.toggleFunction = function toggleFunction() {
  var x = document.getElementById('navDemo')
  if (x.className.indexOf('show') == -1) {
    x.className += ' show'
  } else {
    x.className = x.className.replace(' show', '')
  }
}
