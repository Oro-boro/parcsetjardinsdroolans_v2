<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Image;
use App\Entity\Service;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        //insertion de faker
        $faker = Factory::create('fr_FR');

        //insert utilisateur --------------------------------------------
        $user = new User();
        $user
            ->setUsername('admintest')
            ->setRoles(['ROLE_ADMIN'])
            ->setNom($faker->lastname())
            ->setPrenom($faker->firstname())
            ->setEmail('User@test.com');

        $password = $this->encoder->encodePassword($user, 'password');
        $user->setPassword($password);
        $manager->persist($user);

        // ---------------------------------------------------------------

        //création de 8 services + images -------------------------------
        // Oui je sais, j'aurais pu faire une boucle et me prendre la tête
        // ou pas, c'est selon sa définition de la chose.

        $service = new Service();
        $service
            ->setNom('Entretien de jardin')
            ->setDescription($faker->text());
        $manager->persist($service);

        //insert de 2 fake images PAR services (Donc 16 images au final)
        for ($i = 0; $i < 2; $i++) {
            $images = new Image();
            $images
                ->setNom($faker->words(2, true))
                ->setFile('placeholder.jpg')
                ->addService($service)
                ->setDescription($faker->text())
                ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                ->setUser($user);
            $manager->persist($images);
        }

        $service = new Service();
        $service
            ->setNom('Clôtures')
            ->setDescription($faker->text());
        $manager->persist($service);

        //insert de 2 fake images PAR services (Donc 16 images au final)
        for ($i = 0; $i < 2; $i++) {
            $images = new Image();
            $images
                ->setNom($faker->words(2, true))
                ->setFile('placeholder.jpg')
                ->addService($service)
                ->setDescription($faker->text())
                ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                ->setUser($user);
            $manager->persist($images);
        }

        $service = new Service();
        $service
            ->setNom('Terrasse')
            ->setDescription($faker->text());
        $manager->persist($service);

        //insert de 2 fake images PAR services (Donc 16 images au final)
        for ($i = 0; $i < 2; $i++) {
            $images = new Image();
            $images
                ->setNom($faker->words(2, true))
                ->setFile('placeholder.jpg')
                ->addService($service)
                ->setDescription($faker->text())
                ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                ->setUser($user);
            $manager->persist($images);
        }

        $service = new Service();
        $service
            ->setNom('Pavages')
            ->setDescription($faker->text());
        $manager->persist($service);

        //insert de 2 fake images PAR services (Donc 16 images au final)
        for ($i = 0; $i < 2; $i++) {
            $images = new Image();
            $images
                ->setNom($faker->words(2, true))
                ->setFile('placeholder.jpg')
                ->addService($service)
                ->setDescription($faker->text())
                ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                ->setUser($user);
            $manager->persist($images);
        }

        $service = new Service();
        $service
            ->setNom('Amenagement de jardin')
            ->setDescription($faker->text());
        $manager->persist($service);

        //insert de 2 fake images PAR services (Donc 16 images au final)
        for ($i = 0; $i < 2; $i++) {
            $images = new Image();
            $images
                ->setNom($faker->words(2, true))
                ->setFile('placeholder.jpg')
                ->addService($service)
                ->setDescription($faker->text())
                ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                ->setUser($user);
            $manager->persist($images);
        }

        $service = new Service();
        $service
            ->setNom('Abattages et élagages')
            ->setDescription($faker->text());
        $manager->persist($service);

        //insert de 2 fake images PAR services (Donc 16 images au final)
        for ($i = 0; $i < 2; $i++) {
            $images = new Image();
            $images
                ->setNom($faker->words(2, true))
                ->setFile('placeholder.jpg')
                ->addService($service)
                ->setDescription($faker->text())
                ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                ->setUser($user);
            $manager->persist($images);
        }

        $service = new Service();
        $service
            ->setNom('Abris de jardins')
            ->setDescription($faker->text());
        $manager->persist($service);

        //insert de 2 fake images PAR services (Donc 16 images au final)
        for ($i = 0; $i < 2; $i++) {
            $images = new Image();
            $images
                ->setNom($faker->words(2, true))
                ->setFile('placeholder.jpg')
                ->addService($service)
                ->setDescription($faker->text())
                ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                ->setUser($user);
            $manager->persist($images);
        }

        $service = new Service();
        $service
            ->setNom('Points d\'eau')
            ->setDescription($faker->text());
        $manager->persist($service);
        //insert de 2 fake images PAR services (Donc 16 images au final)
        for ($i = 0; $i < 2; $i++) {
            $images = new Image();
            $images
                ->setNom($faker->words(2, true))
                ->setFile('placeholder.jpg')
                ->addService($service)
                ->setDescription($faker->text())
                ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                ->setUser($user);
            $manager->persist($images);
        }
        // ---------------------------------------------------------------
        /*
        //création d'UN service POUR LA PARTIE TEST CODE COVERAGE (dans la DB de TEST)
        $service = new Service();
        $service
            ->setNom('service TEST')
            ->setDescription($faker->words(10, true));
        $manager->persist($service);

        //insert d'UNE image POUR LA PARTIE TEST CODE COVERAGE (dans la DB de TEST)
        $images = new Image();
        $images
            ->setNom('image TEST')
            ->setFile('placeholder.jpg')
            ->addService($service)
            ->setDescription($faker->text())
            ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
            ->setUser($user);
        $manager->persist($images);
        */
        // ---------------------------------------------------------------

        $manager->flush();
    }
}
