<?php

namespace App\ContactService;

use DateTime;
use App\Entity\Contact;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ContactService
{
    private $manager;
    private $session;

    public function __construct(EntityManagerInterface $manager, SessionInterface $session)
    {
        $this->manager = $manager;
        $this->session = $session;
    }

    public function persistContact(Contact $contact): void
    {
        $contact
            ->setIsSend(false)
            ->setCreatedAt(new DateTime('now'));
        $this->manager->persist($contact);
        $this->manager->flush();
        /** @var FlashBagInterface $flashBag */
        $flashBag = $this->session->getBag('flashes');
        $flashBag->add("success", "Message bien envoyé");
    }

    public function isSend(Contact $contact): void
    {
        $contact->setIsSend(true);
        $this->manager->persist($contact);
        $this->manager->flush();
    }
}
