<?php

namespace App\Repository;

use App\Entity\Image;
use App\Entity\Service;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Image|null find($id, $lockMode = null, $lockVersion = null)
 * @method Image|null findOneBy(array $criteria, array $orderBy = null)
 * @method Image[]    findAll()
 * @method Image[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Image::class);
    }

    /**
     * @return Image[] Returns an array of Images objects
     */
    public function serviceOrganisationImages()
    {
        return $this->createQueryBuilder('i')
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(6)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Image[] Returns an array of Images objects
     */
    public function findAllImages(Service $service): array
    {
        return $this->createQueryBuilder('s')
            ->where(':service MEMBER OF s.service')
            ->setParameter('service', $service)
            ->getQuery()
            ->getResult();
    }
}
