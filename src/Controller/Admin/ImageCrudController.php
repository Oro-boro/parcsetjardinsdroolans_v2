<?php

namespace App\Controller\Admin;

use App\Entity\Image;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ImageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Image::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(["createdAt" => 'DESC']);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nom', 'Nom de l\'image')
                ->setHelp('Cette information n\'apparait pas sur le site et n\'est utile qu\'a l\'administrateur.'),
            TextareaField::new('description')
                ->setHelp('Cette information n\'apparait pas sur le site et n\'est utile qu\'a l\'administrateur.'),
            DateField::new('createdAt', 'Date de création')
                ->onlyOnIndex(),
            // "Check onlywhencreating" a virer ou pas.
            TextField::new('imageFile')
                ->setFormType(VichImageType::class)
                ->hideOnIndex()
                ->setTranslationParameters(['form.label.delete' => 'Supprimer image uniquement.']),
            ImageField::new('file', 'Aperçu')
                ->setBasePath('/uploads/images/')
                ->onlyOnIndex(),
            AssociationField::new('service')->hideOnIndex()
                ->setHelp('Selectionnez le Service en rapport avec l\'image. <br>
                            Si "vide", l\'image ne sera affichée QUE dans la Gallerie générale.'),
            ArrayField::new('service')->onlyOnIndex(),
        ];
    }
}
