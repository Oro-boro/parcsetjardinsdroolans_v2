<?php

namespace App\Controller\Admin;

use App\Entity\Image;
use App\Entity\Service;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/interfaceAdministration.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('- Interface Administration - Parcs et Jardins Droolans');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Retour au site', 'fa fa-home');
        yield MenuItem::linkToCrud('Gestion des Services', 'fas fa-newspaper', Service::class);
        yield MenuItem::linkToCrud('Gestion des Images', 'fas fa-palette', Image::class);
    }
}
