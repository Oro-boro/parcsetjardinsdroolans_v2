<?php

namespace App\Controller;

use App\Repository\ServiceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home(ServiceRepository $serviceRepository): Response
    {
        return $this->render('home/home.html.twig', [
            'services' => $serviceRepository->findAll()
        ]);
    }
}
