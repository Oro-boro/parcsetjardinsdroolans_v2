<?php

namespace App\Controller;

use App\Repository\ImageRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ImageController extends AbstractController
{
    /**
     * @Route("/gallery", name="gallery")
     */
    public function gallery(
        ImageRepository $imageRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $imageRepository->findby([], ['createdAt' => 'DESC']);
        // ->findby([], ['createdAt' => 'DESC']); Ordre des images dans "gallery"
        $images = $paginator->paginate($data, $request->query->getInt('page', 1), 16);

        return $this->render('image/gallery.html.twig', [
            'images' => $images
        ]);
    }
}
