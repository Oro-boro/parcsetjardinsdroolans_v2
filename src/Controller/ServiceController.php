<?php

namespace App\Controller;

use App\Entity\Service;
use App\Repository\ImageRepository;
use App\Repository\ServiceRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ServiceController extends AbstractController
{
    /**
     * @Route("/services", name="services")
     */
    public function services(ServiceRepository $serviceRepository): Response
    {
        return $this->render('service/services.html.twig', [
            'services' => $serviceRepository->findAll()
        ]);
    }

    /**
     * @Route("/services/{nom}", name="servicesByNom")
     */
    public function servicesByNom(Service $service, ImageRepository $imageRepository): Response
    {
        $images = $imageRepository->findAllImages($service);
        return $this->render('service/template-service.html.twig', [
            'service' => $service,
            'images' => $images,
        ]);
    }
}
