-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 09 sep. 2021 à 18:47
-- Version du serveur :  8.0.23
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `parcsetjardinsdroolans_v2`
--

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `is_send` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id`, `nom`, `email`, `message`, `created_at`, `is_send`) VALUES
(1, 'Jean Edouard', 'yoyo@hotmail.com', 'Bonjour, j\'ai une couille qui pend et les fesses qui claquent.', '2021-09-03 19:35:09', 1),
(2, 'popo', 'test2@hotmail.com', 'bijouuuuuuuuuuuuuur', '2021-09-03 19:44:53', 1);

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210504110534', '2021-05-17 21:40:11', 303),
('DoctrineMigrations\\Version20210504125342', '2021-05-17 21:40:11', 46),
('DoctrineMigrations\\Version20210504132421', '2021-05-17 21:40:11', 294),
('DoctrineMigrations\\Version20210511222024', '2021-05-17 21:40:11', 14),
('DoctrineMigrations\\Version20210514205604', '2021-05-17 21:40:11', 26),
('DoctrineMigrations\\Version20210902133747', '2021-09-02 13:38:06', 54);

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

DROP TABLE IF EXISTS `image`;
CREATE TABLE IF NOT EXISTS `image` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C53D045FA76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `image`
--

INSERT INTO `image` (`id`, `nom`, `created_at`, `description`, `file`, `user_id`) VALUES
(36, 'Template : Clôtures.', '2021-08-30 15:04:33', 'Image \"Service\" : Clôtures.', 'vente-et-pose-de-cloture-612cf381db8de400169791.jpg', 3),
(38, 'Template : Terrasse.', '2021-05-31 01:52:15', 'Image \"Service\" : Terrasse.', 'terrasse-60b4414f1ecbf368443289.jpg', 3),
(42, 'Template : Aménagement de jardin.', '2021-05-31 01:51:41', 'Image \"Service\" : Aménagement de jardin.', 'amenagement-de-jardin-60b4412d6461c129031000.jpg', 3),
(43, 'Template : Entretien de jardin.', '2021-05-31 01:52:04', 'Image \"Service\" : Entretien de jardin.', 'entretien-de-jardin-60b441443b809872784397.jpg', 3),
(48, 'Template : Points d\'eau.', '2021-05-31 01:52:44', 'Image \"Service\" : Points d\'eau.', 'creation-de-points-d-eau-60b4416c9f9da580652198.jpg', 3),
(51, 'Template : Pose de châlets.', '2021-06-17 19:43:50', 'Image \"Service\" : Abris de jardins.', 'pose-de-chalet-60cba5f6d19f4661653875.jpg', 3),
(52, 'Template : Abattages et élagages.', '2021-06-17 19:51:57', 'Image \"Service\" : Abattages et élagages.', 'abattages-et-elagages-d-arbres-60cba7dde696d509480244.jpg', 3),
(53, 'Template : Pavages.', '2021-06-17 19:59:56', 'Image \"Service\" : Pavages.', 'pavages-60cba9bcf03b3790226764.jpg', 3),
(54, 'image TEST - entretien de jardin', '2021-06-18 00:16:22', 'blabla', '562-1024x768-60cbe5d606b0c182494727.jpg', 3),
(57, 'Image Terrasse 2', '2021-06-21 19:06:35', 'Création terrasse', '570-1024x768-60d0e33b3ac94399816221.jpg', 3),
(58, 'Image Abattages et élagages 2', '2021-06-21 19:16:31', 'Image Abattages et élagages description.', '566-768x1024-60d0e58f3ae41463434559.jpg', 3);

-- --------------------------------------------------------

--
-- Structure de la table `image_service`
--

DROP TABLE IF EXISTS `image_service`;
CREATE TABLE IF NOT EXISTS `image_service` (
  `image_id` int NOT NULL,
  `service_id` int NOT NULL,
  PRIMARY KEY (`image_id`,`service_id`),
  KEY `IDX_748DCD0E3DA5256D` (`image_id`),
  KEY `IDX_748DCD0EED5CA9E6` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `image_service`
--

INSERT INTO `image_service` (`image_id`, `service_id`) VALUES
(36, 20),
(38, 21),
(42, 23),
(43, 19),
(48, 26),
(51, 25),
(52, 24),
(53, 22),
(54, 19),
(57, 21),
(58, 24);

-- --------------------------------------------------------

--
-- Structure de la table `service`
--

DROP TABLE IF EXISTS `service`;
CREATE TABLE IF NOT EXISTS `service` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description2` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `service`
--

INSERT INTO `service` (`id`, `nom`, `description`, `description2`) VALUES
(19, 'Entretien de jardin', 'Maintenir l’éclat de votre jardin tout au long de l’année nécessite un soin minutieux.', '<div>N’ayez crainte de confier l’entretien de votre jardin aux Parcs et jardins Droolans.&nbsp;<br>Sachez que nous pouvons aussi prendre en charge la conception, la création et la réalisation du jardin de vos rêves. Tous nos jardiniers paysagistes possèdent les compétences nécessaires pour assurer un entretien respectueux du cycle du végétal selon les saisons.<br><br></div><h1>Adressez-vous à nous pour des services de tonte et de scarification de pelouse.&nbsp;</h1><div><br>Confiez-nous les tailles de vos haies de petites ou grandes tailles. Nous nous occupons aussi bien de grands que de petits travaux d’entretien allant de ramassage de feuilles aux débroussaillages et fauchages.&nbsp;<br><br>Sans oublier les désherbages, les entretiens de massifs et les traitements nécessaires suivant les saisons.</div>'),
(20, 'Clôtures', 'Nos différentes clôtures se choisissent en harmonie avec votre maison pour contribuer à l’esthétique de l’ensemble.', '<div>Installer une clôture nécessite un savoir-faire spécifique. D’où la nécessité de solliciter l’intervention d’un expert pour assurer un résultat solide et durable. Des ouvriers compétents effectueront des travaux de pose qui sera réalisée dans les règles de l’art.&nbsp;</div><h1><br>Confiez la pose de votre clôture à un professionnel&nbsp;</h1><div><br>Engager un entrepreneur pour des travaux de pose de clôture présente plusieurs avantages. Il vous donnera des conseils avisés et vous aide ainsi à faire le choix le plus judicieux en matière de clôture. Il vous conseillera sur le produit le plus efficace ainsi que le matériau le plus adéquat à votre situation. Il sera attentif à vos désirs en tenant compte du moindre de vos besoins. Lors de la conception de votre clôture, votre situation financière sera également prise en compte. Rien ne sera décidé sans votre consentement.&nbsp;<br>Fermer un terrain à l’aide d’une clôture exige, selon les cas, l’obtention d’une autorisation des travaux auprès de la commune. Un entrepreneur expérimenté saura vous accompagner dans les démarches administratives à suivre, en cas de besoin. Pour une pose clôture en Brabant Wallon, n’hésitez pas à faire appel à Parcs et jardins Droolans.&nbsp;<br>Notre entreprise wallonne mettra à votre service une équipe qualifiée pour un travail bien fait et satisfaisant.<br><br></div><h1>Différents types de clôtures</h1><div><br>Nous vous garantissons une pose effectuée dans les règles de l’art pour un résultat solide et durable. Les ouvriers employés par les Parcs et jardins Droolans mis à votre disposition, sont aptes à travailler avec différentes marques de matériaux, présentes sur le marché. Vous êtes alors libre d’opter pour le matériau de votre choix afin de réaliser la clôture de vos rêves.&nbsp;<br>Nous sommes en mesure d’installer une clôture en bois ou en béton, imitation brique. Si vous préférez des clôtures à remplir ou en nattes de bruyère, nous pouvons aussi nous en charger. Il en va de même pour la toiture traditionnelle en treillis souples ou rigides.</div>'),
(21, 'Terrassement', 'De la terrasse aux abords de votre piscine, nous nous engageons à créer des espaces extérieurs fonctionnels lesquels reflètent votre personnalité.', '<div>Au retour des beaux jours, l’aménagement de terrasse est un projet justifié pour y passer des moments de partage. Aux Parcs et jardins Droolans, cette intervention est réalisée par une équipe de professionnels qualifiés toujours à votre écoute pour mieux choisir le matériau qui convient le plus à vos besoins et aux différentes activités que vous prévoyez pour mieux profiter de l’été.&nbsp;<br>Si la terrasse en pierres est privilégiée pour sa durabilité et sa facilité d’entretien, celle en bois n’est pas en reste pour un résultat naturel et chaleureux.&nbsp;<br><br></div><h1>L’aménagement de terrasse en pierres&nbsp;</h1><div><br>Le choix d’une terrasse en pierres s’explique souvent par le besoin d’une installation solide et intemporelle. Là encore, vous pouvez compter sur le savoir-faire de notre équipe pour vous conseiller sur le type de pierre qui convient le mieux. Ainsi, si vous avez envie d’une terrasse résistante aux intempéries tout en vous garantissant un effet décoratif assuré, les dalles de pierre reconstituée sont les plus adaptées. Et lorsqu’elles sont couplées au gravier, elles permettent de faire passer l’eau pour une terrasse plus résistante aux effets du ruissellement de l’eau.&nbsp;<br>Par contre, si vous misez sur l’effet rustique de votre terrasse en pierres, il vaut mieux opter pour des pavés de granit ou de marbre. Pour une touche plus moderne, notre équipe d’experts en aménagement extérieur peut également vous proposer une terrasse en grès cérame. Les autres possibilités pour l’aménagement d’une terrasse en pierres sont l’ardoise, le basalte et le calcaire, donc le choix dépendra de votre type de sol et de vos besoins en termes d’aspect décoratif.&nbsp;<br><br></div><h1>La terrasse en bois pour une installation chaleureuse&nbsp;</h1><div><br>Parfaitement en harmonie avec les végétaux du jardin, la terrasse en bois apporte une touche de charme naturelle à votre coin de verdure. Aussi, faites confiance à nos experts pour vous proposer les meilleures des solutions pour constituer votre terrasse en bois, que ce soit pour le choix de l’essence de bois comme le pin, le chêne, l’acacia ou le frêne issu des forêts européennes à exploitation durable.&nbsp;<br>Pour une meilleure résistance aux intempéries, votre terrasse en bois subira des traitements écologiques adaptés à vos besoins et au type de finition recherché, et ce, que ce soit pour l’oléothermie à base d’huiles végétales ou la rétification ou traitement par la chaleur. Outre le bois naturel, vous pourrez également compter sur les prouesses d’un bois composite imputrescible et 100 % recyclable pour l’aménagement de votre terrasse en bois.</div>'),
(22, 'Pavages', 'Nous disposons d’une large gamme de matériaux de qualité qui ne manquera pas de vous surprendre et de convenir parfaitement à vos envies.', '<h1>Pavages, allées de garage, terrassement et pose de bordure</h1><div><br>Le pavage embellira fortement votre jardin et deviendra un endroit d\'évasion privilégié.&nbsp;<br><br>Votre jardin mérite des produits d\'exception, c\'est pour cela que les Parcs et jardins Droolans vous garantit un savoir-faire conjugué à la qualité des matériaux.&nbsp;</div><div><br>Que votre allée soit en ligne droite, courbes en nuances ou en rosaces, que vos pavés soient carré, arrondi ou ondulé, Parcs et jardins Droolans est votre partenaire afin de créer sur mesure votre espace vert.</div>'),
(23, 'Amenagement de jardin', 'Vous désirez un aménagement des abords de votre maison en harmonie avec l’architecture.', '<div>A la recherche d’une société spécialisé dans l’entretien de jardin dans la région du Brabant Wallon ? Faites appel aux Parcs et jardins Droolans ! Notre entreprise de jardinage met à votre service une équipe de jardiniers qualifiés. Nous prenons en charge l’ensemble de travaux ayant trait à l’entretien de tous les espaces verts.&nbsp;<br><br></div><h1>Pourquoi faire appel à nous et à notre entreprise ?&nbsp;</h1><div><br>Une de nos qualités est l’attention particulière accordée au ressenti des clients par rapport à leur jardin. Nous prenons soin de créer et d’entretenir un espace vert selon les attentes du propriétaire. Notre entreprise de jardin n’est certes pas une grande société, mais notre taille humaine est un atout en ayant la possibilité de vous proposer un devis au prix le plus juste et le plus adapté à vos besoins.<br><br>Il vous est aussi possible de signer avec notre entreprise un contrat d’entretien de jardin, annuel et personnalisé. Nous garantissons un travail satisfaisant pour un jardin propre et bien entretenu. Nous disposons également de tous le matériel de jardinage nécessaires aussi bien pour la tonte que pour l’élagage et l’abattage d’arbre, etc.</div>'),
(24, 'Abattages et élagages', 'Les arbres nécessitent un élagage régulier, nous garantissons la sécurité de l\'opération de A à Z.', '<div>Vous avez des arbres imposants dans votre jardin ?<br><br>Notre équipe de professionnels qualifiés intervient dans le Brabant Wallon pour l’élagage des arbres avec des parties malades ou des branches mortes. L’élagage est également la solution adaptée pour vos arbres ornementaux dont les coupes serviront essentiellement à en améliorer la silhouette en fonction du sens du vent, du niveau d’ensoleillement et du résultat esthétique recherché.&nbsp;<br><br>Et lorsque l’abattage de l’arbre devient inévitable pour des soucis de sécurité pour vous et celle du voisinage, nous mettons à votre service notre savoir-faire tout en respectant les réglementations imposées.&nbsp;<br><br></div><h1>Quand procéder à l’élagage des arbres ?&nbsp;</h1><div><br>Notre équipe de professionnels qualifiés se déplace chez vous pour effectuer les travaux d’élagage de vos arbres devenus trop imposants. Cette intervention est également préconisée avant l’arrivée des beaux jours pour réussir à orienter le sens de la repousse, garantissant ainsi un effet plus esthétique suivant les variétés de vos végétaux. Dans un souci sécuritaire, l’élagage des arbres sert essentiellement à éliminer les branches qui atteignent les toitures et les fils électriques à haute tension pour préserver la maison et celles du voisinage, et ce, en application des lois en vigueur en Brabant Wallon.&nbsp;<br><br>Quoi qu’il en soit, faites appel à nous pour vous assurer que les interventions seront réalisées par des élagueurs compétents et soucieux de l’équilibre naturel de vos arbres, dont les branches malades seront diagnostiquées avant de procéder à une coupe précise par des matériels performants et fiables.&nbsp;<br><br></div><h1>L’abattage d’arbres, une intervention sécurisée&nbsp;</h1><div><br>Lorsque notre équipe est face à un vieil arbre dont les dangers imminents ont été prouvés, elle procède à son abattage. Une intervention stratégique qui nécessite tout un travail de sécurisation des environs avant de procéder à l’abattage proprement dit, et dont les détails de chaque intervention vous seront expliqués pour vous permettre de mieux comprendre les étapes à suivre et les consignes de sécurité à respecter. Les branches, les troncs et les déchets végétaux seront ensuite évacués de votre propriété et nous nous chargeons du broyage. Ainsi, vous pouvez vous assurer que votre jardin retrouve son état initial après l’abattage de l’arbre.&nbsp;<br><br>Que ce soit pour l’élagage ou l’abattage de vos arbres, faites donc appel à notre équipe de professionnels qualifiés pour effectuer un diagnostic préalable avant de choisir la technique de coupe la mieux adaptée. Qui plus est, les variétés des arbres sont prises en compte avant de procéder à l’élagage ou à l’abattage.&nbsp;<br><br></div><h1>Vous pouvez compter sur nos experts pour :</h1><div><br></div><ul><li>Identifier les branches malades ou endommagées avant de les enlever&nbsp;</li><li>Tailler l’arbre avec précision, en respectant l’âge de l’arbre et ses caractéristiques, parce que les bourgeons d’un jeune arbre en croissance seront situés sur la face inférieure de ses rameaux, tandis que le cas contraire se manifeste chez un vieil arbre.&nbsp;</li><li>Sélectionner les arbres potentiellement dangereux et appliquer les méthodes de sécurisation avant l’abattage&nbsp;</li><li>Évacuer les branches et les troncs d’arbres coupés avant de procéder au broyage&nbsp;</li><li>Garantir une intervention propre et rapide pour que votre jardin ou votre cour se retrouve dans son état initial, sans qu’aucune branche, feuille ne soit éparpillée sur sa surface.</li></ul>'),
(25, 'Abris de jardins', 'Pour stocker des outils ou du matériel de jardinage ou encore pour protéger du mobilier de jardin, les abris proposent des fonctions très pratiques.', '<h1>Vente et pose d\'abris de jardin</h1><div><br>L\'abri de jardin s\'est imposé comme un indispensable dans l\'univers de l\'extérieur et du jardinage. Que ce soit pour aider à stocker outils, matériels de jardinage ou encore pour protéger du mobilier de jardin, les abris proposent des fonctions très pratiques.&nbsp;<br><br>Cependant, choisir un abri de jardin peut-être compliqué : entre la matière utilisée, la surface souhaitée ou encore la couverture de la toiture.&nbsp;<br><br>Parcs et jardins Droolans conseille, vend et installe votre abri de jardin.</div>'),
(26, 'Points d\'eau', 'Dans votre jardin, tout ne se passe pas que dans la terre …beau et utile : n’hésitez plus à aménager un point d’eau.', '<div>Nos jardiniers paysagistes connaissent les ficelles du métier de jardinage. Ils sont polyvalents en étant à la fois, bricoleur et manuel avec une bonne condition physique. Ils sont aussi multitâches et maitrisent la technique exigée pour une simple tonte de pelouse, à l’élagage d’arbre en passant par la taille des végétaux.&nbsp;<br><br>Ils peuvent aussi s’occuper de maçonnerie, de fontaineries et d’aménagement de plan d’eau.</div>');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(180) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `roles`, `password`, `nom`, `prenom`, `email`) VALUES
(3, 'admintest', 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}', '$argon2id$v=19$m=65536,t=4,p=1$MUhHdFlmY3FBL1JVU2pSOA$hz7lx0VegSbKobcK8QPdKn1Erx6mrTgiXJwGOZRf1po', 'Thibault', 'Véronique', 'User@test.com');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `FK_C53D045FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `image_service`
--
ALTER TABLE `image_service`
  ADD CONSTRAINT `FK_748DCD0E3DA5256D` FOREIGN KEY (`image_id`) REFERENCES `image` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_748DCD0EED5CA9E6` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
