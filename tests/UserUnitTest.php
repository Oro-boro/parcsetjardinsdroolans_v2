<?php

namespace App\Tests;


use App\Entity\User;
use App\Entity\Image;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new User();
        $user
            ->setUsername('usernametest')
            ->setEmail('true@test.com')
            ->setNom('nom')
            ->setPrenom('prenom')
            ->setPassword('password')
            ->setRoles(['ROLE_TEST']);

        $this->assertTrue($user->getUsername() === 'usernametest');
        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getNom() === 'nom');
        $this->assertTrue($user->getPrenom() === 'prenom');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getRoles() === ['ROLE_TEST', 'ROLE_USER']);
    }

    public function testIsFalse()
    {
        $user = new User();
        $user
            ->setUsername('usernametest')
            ->setEmail('true@test.com')
            ->setNom('nom')
            ->setPrenom('prenom')
            ->setPassword('password');

        $this->assertFalse($user->getUsername() === 'false');
        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getNom() === 'false');
        $this->assertFalse($user->getRoles() === 'false');
        $this->assertFalse($user->getPrenom() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
    }

    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getId());
        $this->assertEmpty($user->getUsername());
        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getPassword());
    }

    public function testRemoveAddGetImagesUser()
    {
        $user = new User();
        $images = new Image();

        $this->assertEmpty($user->getImages());

        $user->addImage($images);
        $this->assertContains($images, $user->getImages());

        $user->removeImage($images);
        $this->assertEmpty($user->getImages());
    }
}
