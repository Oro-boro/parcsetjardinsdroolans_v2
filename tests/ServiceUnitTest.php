<?php

namespace App\Tests;

use App\Entity\Image;
use App\Entity\Service;
use PHPUnit\Framework\TestCase;

class ServiceUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $Services = new Service();
        $Services
            ->setNom('nom')
            ->setDescription('description');

        $this->assertTrue($Services->getNom() === 'nom');
        $this->assertTrue($Services->getDescription() === 'description');
    }

    public function testIsFalse()
    {
        $Services = new Service();
        $Services
            ->setNom('nom')
            ->setDescription('description');

        $this->assertFalse($Services->getNom() === 'false');
        $this->assertFalse($Services->getDescription() === 'false');
    }

    public function testIsEmpty()
    {
        $Services = new Service();

        $this->assertEmpty($Services->getNom());
        $this->assertEmpty($Services->getDescription());
        $this->assertEmpty($Services->getId());
    }
}
