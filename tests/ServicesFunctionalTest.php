<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ServicesFunctionalTest extends WebTestCase
{
    public function testDisplayServices()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/services');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorExists('h4', 'Clôtures');
    }

    public function testDisplayOneService()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/services/service TEST');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'service TEST');
    }
}
