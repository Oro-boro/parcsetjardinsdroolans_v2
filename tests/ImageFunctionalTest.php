<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EnImagesFunctionalTest extends WebTestCase
{
    public function testDisplayImages()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/gallery');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'En images');
    }

    public function testDisplayImageByNom()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/gallery');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('p', 'et quidem');
    }
}
