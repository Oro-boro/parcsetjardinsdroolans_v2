<?php

namespace App\Tests;

use App\Entity\Image;
use App\Entity\Service;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class ImagesUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $images = new Image();
        $services = new Service();
        $user = new User();
        $datetime = new Datetime();

        $images
            ->setNom('nom')
            ->setCreatedAt($datetime)
            ->setDescription('description')
            ->setFile('file')
            ->addService($services)
            ->setUser($user);

        $this->assertTrue($images->getNom() === 'nom');
        $this->assertTrue($images->getCreatedAt() === $datetime);
        $this->assertTrue($images->getDescription() === 'description');
        $this->assertTrue($images->getFile() === 'file');
        $this->assertContains($services, $images->getService());
        $this->assertTrue($images->getUser() === $user);
    }

    public function testIsFalse()
    {
        $images = new Image();
        $services = new Service();
        $user = new User();
        $datetime = new Datetime();

        $images
            ->setNom('nom')
            ->setCreatedAt($datetime)
            ->setDescription('description')
            ->setFile('file')
            ->addService($services)
            ->setUser($user);

        $this->assertFalse($images->getNom() === 'false');
        $this->assertFalse($images->getCreatedAt() === new datetime());
        $this->assertFalse($images->getDescription() === 'false');
        $this->assertFalse($images->getFile() === 'false');
        $this->assertNotContains(new service(), $images->getService());
        $this->assertFalse($images->getUser() === new User());
    }

    public function testIsEmpty()
    {
        $images = new Image();

        $this->assertEmpty($images->getId());
        $this->assertEmpty($images->getNom());
        $this->assertEmpty($images->getCreatedAt());
        $this->assertEmpty($images->getDescription());
        $this->assertEmpty($images->getFile());
        $this->assertEmpty($images->getService());
        $this->assertEmpty($images->getUser());
    }

    public function testGetRemoveAddImagesServices()
    {
        $services = new Service();
        $images = new Image();

        $this->assertEmpty($services->getImages());

        $services->addImage($images);
        $this->assertContains($images, $services->getImages());

        $services->removeImage($images);
        $this->assertEmpty($services->getImages());
    }
}
